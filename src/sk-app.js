

import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkApp extends SkElement {

    get cnSuffix() {
        return 'app';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get routes() {
        if (! this._routes) {
            this._routes = JSON.parse(this.confValOrDefault('routes', '{}'));
        }
        return this._routes;
    }

    set routes(routes) {
        this._routes = routes;
        this.setAttribute(JSON.stringify(this._routes));
    }

    get slot() {
        if (! this._slot) {
            this._slot = this.el.querySelector('slot');
        }
        return this._slot;
    }

}
