

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';


export class SkAppImpl extends SkComponentImpl {

    get suffix() {
        return 'app';
    }

    dynamicImportSupported() {
        try {
            new Function('import("")');
            return true;
        } catch (err) {
            return false;
        }
    }

    setupRouter() {
        let root = this.comp.confValOrDefault('ro-root', "/");
        let useHash = this.comp.confValOrDefault( 'ro-use-hash', false);
        this.router = new Navigo(root, { hash: (useHash === 'true') });
        for (let route of Object.keys(this.comp.routes)) {
            let path = this.comp.routes[route];
            if (typeof path === 'string') {
                if (customElements.get(this.comp.routes[route])) {
                    this.router.on(route, function (event) {
                        this.comp.dispatchEvent(new CustomEvent('skroutestart', { target: this.comp, detail: { value: route }, bubbles: true, composed: true }));
                        this.renderCompByRoute(path);
                        this.comp.dispatchEvent(new CustomEvent('skrouteend', { target: this.comp, detail: { value: route }, bubbles: true, composed: true }));
                    }.bind(this));
                } else {
                    this.logger.warn('Attempt to use unregistred custom element for route: ', path);
                }
            } else {
                for (let key of Object.keys(path)) {
                    if (path[key].indexOf('.js') > 0) {
                        if (this.dynamicImportSupported() && this.comp.getAttribute('dimport') !== 'false') {
                            import(path[key]).then((m) => {
                                this.router.on(route, function (event) {
                                    this.comp.dispatchEvent(new CustomEvent('skroutestart', { target: this.comp, detail: { value: route }, bubbles: true, composed: true }));
                                    let tagName = 'sk-' + key.toLowerCase();
                                    this.renderCompByRoute(tagName, m[key]);
                                    this.comp.dispatchEvent(new CustomEvent('skrouteend', { target: this.comp, detail: { value: route }, bubbles: true, composed: true }));
                                }.bind(this));
                                this.router.resolve();
                            });
                        } else {
                            this.router.on(route, function (event) {
                                try {
                                    let def = (Function('return ' + key))();
                                    this.comp.dispatchEvent(new CustomEvent('skroutestart', {
                                        target: this.comp,
                                        detail: { value: route },
                                        bubbles: true,
                                        composed: true
                                    }));
                                    let tagName = 'sk-' + key.toLowerCase();
                                    this.renderCompByRoute(tagName, def);
                                    this.comp.dispatchEvent(new CustomEvent('skrouteend', {
                                        target: this.comp,
                                        detail: { value: route },
                                        bubbles: true,
                                        composed: true
                                    }));
                                } catch {
                                    let scriptPromise = new Promise((resolve, reject) => {
                                        let script = document.createElement('script');
                                        script.onload = resolve;
                                        script.onerror = reject;
                                        script.async = true;
                                        script.src = path[key];
                                        document.body.appendChild(script);
                                    });
                                    scriptPromise.then(function () {
                                        this.comp.dispatchEvent(new CustomEvent('skroutestart', {
                                            target: this.comp,
                                            detail: { value: route },
                                            bubbles: true,
                                            composed: true
                                        }));
                                        let tagName = 'sk-' + key.toLowerCase();
                                        let def = (Function('return ' + key))();
                                        this.renderCompByRoute(tagName, def);
                                        this.comp.dispatchEvent(new CustomEvent('skrouteend', {
                                            target: this.comp,
                                            detail: { value: route },
                                            bubbles: true,
                                            composed: true
                                        }));
                                    }.bind(this));
                                }
                            }.bind(this));
                        }
                    }
                }
            }
        }
        this.router.resolve();
    }

    renderCompByRoute(tagName, comp) {
        if (comp && ! customElements.get(tagName)) {
            customElements.define(`${tagName}`, comp);
        }
        this.comp.slot.innerHTML = '';
        this.comp.slot.insertAdjacentHTML('beforeend', `<${tagName}></${tagName}>`);
    }

    async afterRendered() {
        super.afterRendered();
        this.logger.debug('app after rendered, config routes', this.comp.configEl);
        if (Object.keys(this.comp.routes).length > 0) {
            this.setupRouter();
        }
    }

}